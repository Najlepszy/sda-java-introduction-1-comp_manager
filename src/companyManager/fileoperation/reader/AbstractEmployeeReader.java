package companyManager.fileoperation.reader;

import companyManager.Employee;

public abstract class AbstractEmployeeReader implements EmployeeReader {

    protected String pathToFile;

    protected AbstractEmployeeReader(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    @Override
    public abstract Employee[] readEmployees();
}
